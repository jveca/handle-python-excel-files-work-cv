import pandas as pd
from pandas.api.types import is_numeric_dtype

# from tabulate import tabulate

# the output header
out_headers_excel = ['Select (x)','Tag','archiving','compdevpercent','descriptor','digitalset','displaydigits','engunits','excdevpercent','instrumenttag','location1','location2','location3','location4','pointsource','pointtype','span','step','zero']

def rename_header(input_df): # compare input headers and fix with the desired output headers
    columns_list = input_df.columns  # to headers

    for col_in in columns_list: # iterate over input columns
        for out_col in out_headers_excel: # iterate over output columns
            if (col_in in out_col or out_col in col_in): # verify if there is a coincidence in headers
                input_df.rename(columns={col_in: out_col}, inplace=True) # rename columns headers

    return input_df

def check_field_archiving (input_check): # change value from 'archiving' field

    # add new validation, to perform if archiving field is correct
    if not (is_numeric_dtype(input_check['archiving'])): # check the column type

        input_check['archiving'] = input_check['archiving'].str.replace('ON','1') # replace values    

        input_check["archiving"] = input_check["archiving"].apply(pd.to_numeric) # cast to numeric type 

    return input_check

def check_area_tag_pi(input):

    """  RULES
    - PITOPI_C2 (Interfaz 4) - - - - - Tags del Sistema (UFO2, Device Status, Heartbeat, etc)
    - (*)PITOPI_C2_1 (Interfaz 3) - - - Área Seca (3100 - 3200)
    - (*)PITOPI_C2_2 (Interfaz 5) - - - Área Húmeda (3300 a más)
    - (*)PITOPI_C2_3 (Interfaz 9) - - - Otros Sistemas (EMS, P&T, Production Manager, Mina)

    - PITOPI_C1 (Interfaz 6) - - - - - Área Seca (3100 - 3200) y otros tags (ACE, FIMs, etc.)
    - (*)PITOPI_C1_1 (Interfaz 10) - - - Área Húmeda (3300 a más)
    - PITOPI_HIDRO (Interfaz 7) - - - Hidro

    Excepciones
        - PITOPI_C2 -> Falta (UFO2, Device Status, Heartbeat, etc)
        - PITOPI_C1 -> Falta la validacion previa de (ACE, FIMs, etc.)
    """

    input['AreaTagged'] = ''  #create new col to store the Tag's area
    input['Source'] = ''  #create new col to store the Tag's source business
    input['point_source_pcn'] = ''  #create new col to store the Tag's point source pcn

    # cast to tag value to UPPER 
    input['Tag'] = input['Tag'].str.upper() 

    for ind in input.index: 
        tag_identifier = input['Tag'][ind]
        tag_identifier = tag_identifier[:5] # extract until the 4th position
        interface = tag_identifier[:2] # extract the interfeace of tag 
        area = tag_identifier[3:5] # extract the area number of tag
        
        # define rules to check area                
        if (interface in 'C1'): # check C1 tags
            input['point_source_pcn'][ind] = 'HW'

            if ( int(area) >= 31 and int(area)<=32 ):
                input['AreaTagged'][ind] = 6
                input['Source'][ind] = 'PITOPI_C1'
            if ( int(area)>=33 ):
                input['AreaTagged'][ind] = 10
                input['Source'][ind] = 'PITOPI_C1_1' 
            
        elif (interface in 'C2'): # check C2 tags
            input['point_source_pcn'][ind] = 'DV'

            if ( int(area) >= 31 and int(area)<=32 ):
                input['AreaTagged'][ind] = 3
                input['Source'][ind] = 'PITOPI_C2_1'
            if ( int(area)>=33 ):
                input['AreaTagged'][ind] = 5
                input['Source'][ind] = 'PITOPI_C2_2'
            else:
                input['AreaTagged'][ind] = 9
                input['Source'][ind] = 'PITOPI_C2_3'
        else:  # check Hidro Tags
            input['point_source_pcn'][ind] = 'OPC_HIDRO'

            input['AreaTagged'][ind] = 7
            input['Source'][ind] = 'PITOPI_HIDRO'
            
    return input

def to_business(input_business):
    input_business['pointsource'] = input_business['Source'] # copy values 
    del input_business['Source'] # delete column

    columns_business = input_business.columns # extract only headers 
    out_list = columns_business.values.tolist() # cast 
    out_list = set(out_headers_excel)-set(out_list) # extract the unique values from the comparison between output and input headers

    for out_it in out_list:
        input_business.insert(2, out_it, "")  # add new headers to frame
    
    input_business['location1'] = input_business['AreaTagged'] # copy values
    del input_business['AreaTagged'] # delete column

    input_business['location2'] = 0 # set value
    input_business['location3'] = 1 # set value
    input_business['Select (x)'] = 'x' # set value

    return input_business


def check_PCN_area(area, interface): # to get the area based on interface
    # switcher to choose the area from C1
    switcher_c1 ={
        34: 4,
        32: 5,
        33: 6,
        35: 7,
        36: 8,
        31: 9,
        37: 10,
        38: 11,
        39: 12,
        51: 13,
        54: 14,
    }
    # switcher to choose the area from C2
    switcher_c2 ={
        31: 4,
        32: 5,
        33: 6,
        34: 7,
        35: 8, # the same
        36: 8, # the same
        37: 9,
        38: 10,
        39: 11,
        51: 12,
        66: 13,
    }
    if (interface in 'C1'):
        return switcher_c1.get(area,"Invalid area")
    if (interface in 'C2'):
        return switcher_c2.get(area,"Invalid area")

# print(check_c1_area(34,'C1'))


def to_pcn(input_pcn):
    '''
    RULES
    C1
     - 3400 -> 4
     - 3200 -> 5
     - 3300 -> 6
     - 3500 -> 7
     - 3600 -> 8
     - 3100 -> 9
     - 3700 -> 10
     - 3800 -> 11
     - 3900 -> 12
     - 5100 -> 13
     - 5400 -> 14
    C2
    - 3100 -> 4
    - 3200 -> 5
    - 3300 -> 6
    - 3400 -> 7
    - 3500-3600 -> 8
    - 3700 -> 9
    - 3800 -> 10
    - 3900 -> 11
    - 5100 -> 12
    - 6600 -> 13
    HIDRO

    '''

    input_pcn['pointsource'] = input_pcn['point_source_pcn'] # copy values 
    del input_pcn['point_source_pcn'] # delete column

    for ind in input_pcn.index: 
        tag_identifier = input_pcn['Tag'][ind] # get the tag
        tag_identifier = tag_identifier[:5] # extract until the 4th position
        interface = tag_identifier[:2] # extract the interfeace of tag 
        area = tag_identifier[3:5] # extract the area number of tag
        input_pcn['location1'][ind] = int(check_PCN_area(int(area),interface)) # extract the area value based on interface using a switcher

    return input_pcn

    # print(tabulate(input_pcn,headers=input_pcn.columns))


def create_excel(input_to_output_frame_business, input_to_output_frame_pcn, filename_output): # create excel to business
    input_business = input_to_output_frame_business.reindex(columns=out_headers_excel) # reindex columns from the wanted headers
    input_pcn = input_to_output_frame_pcn.reindex(columns=out_headers_excel) # reindex columns from the wanted headers

    # print(tabulate(input_to_output_frame_business,headers=input_to_output_frame_business.columns))

    writer = pd.ExcelWriter(filename_output, engine='xlsxwriter')  # create the excel file
    input_business.to_excel(writer, sheet_name='business',index=False) # create the business sheet
    input_pcn.to_excel(writer, sheet_name='pcn',index=False) # create the pcn sheet

    writer.save()

def process_input_to_output(input, filename_output): 
    input = check_field_archiving(input)  # to change archiving values
    input_check = check_area_tag_pi(input) # checking and adding column based on interface and area given by tag

    output_PCN = output_business = input_check.copy() # copy to output

    output_business = to_business(output_business) # create output_frame_business
    output_PCN = to_pcn(output_business.copy())  # create output_frame_PCN

    create_excel(output_business,output_PCN, filename_output) # create excel file 
    # print(tabulate(input,headers=input.columns))


def main():

    filename_input = 'input.xlsx' # input name
    filename_output = 'salida_consolidado.xlsx' # output name

    input = pd.read_excel(filename_input) 
    columns = input.columns

    input_df = rename_header(input) # to change all the input header 
    process_input_to_output(input_df, filename_output) # process, check, change, and create the ouput excel


if __name__ == "__main__":
    main()


''' 
TO DO:
- create interface to choose input/ouput file
- create a file like a log where we save errors by line
- evaluate another validations:
    - check all cases from rule to business and pcn
'''
